%option noyywrap
%option never-interactive
%option yylineno

%{
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
#include "fun.h"
%}

%x STRING

NUM [0-9]+
INT2 &[bB][01]*
INT8 &[oO](0|([1-7]+[0-7]*))
INT16 &[hH](0|([1-9a-fA-F]+[0-9a-fA-F]*))
ID [a-zA-Z]([_a-zA-Z0-9]{1,254})?
%%

%{
    char comment[1000];
	comment[0] = 0;
%}

^\s*\n printf("EMPTYSTRING\n");


REM([^A-Za-z0-9_\n].*)? printf("Comment : %s\n", yytext+3);
'.* printf("Comment : %s\n", yytext+1);


\" {BEGIN(STRING); comment[0] = 0;}
<STRING>\n		{printf("(%d:0) String constants must end with a double quote.\n",yylineno-1);BEGIN(INITIAL);}
<STRING>\"\"			strcat(comment,"\"");
<STRING>[^\n""]+		strcat(comment,yytext);
<STRING>\" {printf("String literal : %s\n", comment); BEGIN(INITIAL);}


\"[a-zA-Z0-9]\"[cC] {printf("Char: %s\n", yytext);}


Boolean {printf("Type: %s\n", yytext);}
Byte {printf("Type: %s\n", yytext);}
Char printf("Type: %s\n", yytext);
Date printf("Type: %s\n", yytext);
Decimal printf("Type: %s\n", yytext);
Double printf("Type: %s\n", yytext);
Integer printf("Type: %s\n", yytext);
Long printf("Type: %s\n", yytext);
SByte printf("Type: %s\n", yytext);
Short printf("Type: %s\n", yytext);
Single printf("Type: %s\n", yytext);
String printf("Type: %s\n", yytext);
UInteger printf("Type: %s\n", yytext);
ULong printf("Type: %s\n", yytext);
UShort printf("Type: %s\n", yytext);

AddHandler printf("Operator: %s\n", yytext);
AddressOf printf("Operator: %s\n", yytext);
And printf("Operator: %s\n", yytext);
AndAlso printf("Operator: %s\n", yytext);
Call printf("Operator: %s\n", yytext);
Select printf("Operator: %s\n", yytext);
Case printf("Operator: %s\n", yytext);
Try printf("Operator: %s\n", yytext);
Catch printf("Operator: %s\n", yytext);
Finally printf("Operator: %s\n", yytext);
Class printf("Operator: %s\n", yytext);
Const printf("Operator: %s\n", yytext);
Continue printf("Operator: %s\n", yytext);
Declare printf("Operator: %s\n", yytext);
Delegate printf("Operator: %s\n", yytext);
Dim printf("Operator: %s\n", yytext);
DirectCast printf("Operator: %s\n", yytext);
Do printf("Operator: %s\n", yytext);
Loop printf("Operator: %s\n", yytext);
For printf("Operator: %s\n", yytext);
Each printf("Operator: %s\n", yytext);
Next printf("Operator: %s\n", yytext);
If printf("Operator: %s\n", yytext);
Then printf("Operator: %s\n", yytext);
Else printf("Operator: %s\n", yytext);
End printf("Operator: %s\n", yytext);
Enum printf("Operator: %s\n", yytext);
Erase printf("Operator: %s\n", yytext);
On printf("Operator: %s\n", yytext);
Error printf("Operator: %s\n", yytext);
Event printf("Operator: %s\n", yytext);
Exit printf("Operator: %s\n", yytext);
Function printf("Operator: %s\n", yytext);
Get printf("Operator: %s\n", yytext);
GetType printf("Operator: %s\n", yytext);
Imports printf("Operator: %s\n", yytext);
GetXmlNamespace printf("Operator: %s\n", yytext);
GoTo printf("Operator: %s\n", yytext);
Interface printf("Operator: %s\n", yytext);
Is printf("Operator: %s\n", yytext);
IsNot printf("Operator: %s\n", yytext);
Like printf("Operator: %s\n", yytext);
Mod printf("Operator: %s\n", yytext);
Module printf("Operator: %s\n", yytext);
Namespace printf("Operator: %s\n", yytext);
New printf("Operator: %s\n", yytext);
Resume printf("Operator: %s\n", yytext);
Not printf("Operator: %s\n", yytext);
Operator printf("Operator: %s\n", yytext);
OrElse printf("Operator: %s\n", yytext);
Property printf("Operator: %s\n", yytext);
ReDim printf("Operator: %s\n", yytext);
RemoveHandler printf("Operator: %s\n", yytext);
Return printf("Operator: %s\n", yytext);
Set printf("Operator: %s\n", yytext);
Stop printf("Operator: %s\n", yytext);
Structure printf("Operator: %s\n", yytext);
Sub printf("Operator: %s\n", yytext);
SyncLock printf("Operator: %s\n", yytext);
Throw printf("Operator: %s\n", yytext);
TryCast printf("Operator: %s\n", yytext);
TypeOf printf("Operator: %s\n", yytext);
Using printf("Operator: %s\n", yytext);
While printf("Operator: %s\n", yytext);
With printf("Operator: %s\n", yytext);
Xor printf("Operator: %s\n", yytext);
\= printf("Operator: %s\n", yytext);
& printf("Operator: %s\n", yytext);
&= printf("Operator: %s\n", yytext);
\* printf("Operator: %s\n", yytext);
\*= printf("Operator: %s\n", yytext);
\/ printf("Operator: %s\n", yytext);
\/= printf("Operator: %s\n", yytext);
\\ printf("Operator: %s\n", yytext);
\\= printf("Operator: %s\n", yytext);
\^ printf("Operator: %s\n", yytext);
\^= printf("Operator: %s\n", yytext);
\+ printf("Operator: %s\n", yytext);
\+= printf("Operator: %s\n", yytext);
- printf("Operator: %s\n", yytext);
-= printf("Operator: %s\n", yytext);
>  printf("Operator : %s\n", yytext);
\< printf("Operator : %s\n", yytext);
>> printf("Operator : %s\n", yytext);
>>= printf("Operator : %s\n", yytext);
\<< {printf("Operator : %s\n", yytext);}
\<<= {printf("Operator : %s\n", yytext);}
\. printf("Operato r: %s\n", yytext);
, printf("Operator: %s\n", yytext);
: printf("Operator: %s\n", yytext);
\[ printf("Operator: %s\n", yytext);
\] printf("Operator: %s\n", yytext);
\{ printf("Operator: %s\n", yytext);
\} printf("Operator: %s\n", yytext);
\( printf("Operator: %s\n", yytext);
\) printf("Operator: %s\n", yytext);

Alias printf("Clause: %s\n", yytext);
As printf("Clause: %s\n", yytext);
Handles printf("Clause: %s\n", yytext);
Implements printf("Operator or Clause: %s\n", yytext);
In printf("Clause: %s\n", yytext);
Let printf("Clause: %s\n", yytext);
Lib printf("Clause: %s\n", yytext);
Of printf("Clause: %s\n", yytext);

ByRef printf("Modifier: %s\n", yytext);
ByVal printf("Modifier: %s\n", yytext);
Default printf("Modifier: %s\n", yytext);
Friend printf("Modifier: %s\n", yytext);
MustOverride printf("Modifier: %s\n", yytext);
NotInheritable printf("Modifier: %s\n", yytext);
NotOverridable printf("Modifier: %s\n", yytext);
Optional printf("Modifier: %s\n", yytext);
Out printf("Modifier: %s\n", yytext);
Overloads printf("Modifier: %s\n", yytext);
Overridable printf("Modifier: %s\n", yytext);
Overrides printf("Modifier: %s\n", yytext);
ParamArray printf("Modifier: %s\n", yytext);
Private printf("Modifier: %s\n", yytext);
Protected printf("Modifier: %s\n", yytext);
Public printf("Modifier: %s\n", yytext);
Shadows printf("Modifier: %s\n", yytext);
Shared printf("Modifier: %s\n", yytext);
Static printf("Modifier: %s\n", yytext);
WithEvents printf("Modifier: %s\n", yytext);
WriteOnly printf("Modifier: %s\n", yytext);
ReadOnly printf("Modifier: %s\n", yytext);

CBool printf("Function: %s\n", yytext);
CByte printf("Function: %s\n", yytext);
CChar printf("Function: %s\n", yytext);
CDate printf("Function: %s\n", yytext);
CDbl printf("Function: %s\n", yytext);
CDec printf("Function: %s\n", yytext);
CInt printf("Function: %s\n", yytext);
CLng printf("Function: %s\n", yytext);
CObj printf("Function: %s\n", yytext);
CSByte printf("Function: %s\n", yytext);
CShort printf("Function: %s\n", yytext);
CSng printf("Function: %s\n", yytext);
CStr printf("Function: %s\n", yytext);
CUInt printf("Function: %s\n", yytext);
CULng printf("Function: %s\n", yytext);
CUShort printf("Function: %s\n", yytext);

Me printf("KeyWord: %s\n", yytext);
My printf("KeyWord: %s\n", yytext);
MyBase printf("KeyWord: %s\n", yytext);
MyClass printf("KeyWord: %s\n", yytext);
Nothing printf("KeyWord: %s\n", yytext);
Object printf("KeyWord: %s\n", yytext);
Partial printf("KeyWord: %s\n", yytext);
Step printf("KeyWord: %s\n", yytext);
To printf("KeyWord: %s\n", yytext);
True printf("KeyWord: %s\n", yytext);
False printf("KeyWord: %s\n", yytext);
GoSub printf("KeyWord: %s\n", yytext);
EndIf printf("KeyWord: %s\n", yytext);
Variant printf("KeyWord: %s\n", yytext);
Wend printf("KeyWord: %s\n", yytext);
When printf("KeyWord: %s\n", yytext);
Widening printf("KeyWord: %s\n", yytext);

{INT2} {printf("INT2: %d\n", converterInt2(yytext));}
{INT8} {printf("INT8: %d\n", converterInt8(yytext));}
{INT16} {printf("INT16: %d\n", converterInt16(yytext));}
{NUM}I {printf("Number Integer: %d\n", atoi(converterStr(yytext)));}
{NUM} {printf("Number: %d\n", atoi(yytext));}
{ID}% {printf("Integer Identifier: %s\n", converterStr(yytext));}
{ID}\$ {printf("String Identifier: %s\n", converterStr(yytext));}
{ID} {printf("Identifier: %s\n", yytext);}

[A-Za-z_][0-9A-Za-z_]* {
yylval.str=(char *)malloc(strlen(yytext)+1);
strcpy(yylval.str, yytext);
return ID;
}
[1-9][0-9]* {
yylval.ival=atoi(yytext);
return INT;
}
%%
int main(int argc, char *argv[])
{
	if(argc<=1)
	{
		puts("Error");
	}
	else
	{
		fopen_s(&yyin, argv[1], "r");
		yylex();
	}
	_getch();
	return 0;
}