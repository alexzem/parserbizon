%{
# include "stdafx.h"
#include "tree_structs_for_bison.h"
#include <stdio.h>
#include <malloc.h>
void yyerror(char const *s);
extern FILE *yyin;
extern int yylex(void);
struct Expression * create_const_expr(int token);
struct Expression * create_id_expr(char * token);
struct Expression * create_string_expr(char * token);
struct Expression * create_op_expr(enum ExprType Type, struct Expression *lhs, struct Expression *rhs);
struct Statement * create_simple_stmt(enum StmtType type, struct Expression *expr);
struct Statement * create_if_stmt(struct Expression *expr, struct StatementList *block);
struct StatementList * create_stmt_list(struct Statement *s1);
struct StatementList * add_to_stmt_list(struct StatementList *s1, struct StatementList *s2);
struct Statement * create_alt_if(struct Expression *expr, struct StatementList *block);
struct IfList * add_to_if_list (struct StatementList *s1, struct Statement *s2);
struct StatementList * create_if_list (struct Statement *s1);
struct Statement * create_while_stmt(struct Expression *expr, struct StatementList *block);
struct Expression * create_var_expr(enum ExprType Type, struct Expression * expr);
struct Statement * create_fun_stmt(struct Expression * name ,struct StatementList *arg, enum ExprType type,struct StatementList *block);
struct Statement * create_console(enum StmtType type, struct Expression * e1);
struct Expression * create_mass_expr(struct Expression * name, struct Expression * num);
struct Expression * create_func_expr(struct Expression * e1, struct Expression * e2);
struct Expression * create_mass_expr(char * token, int num);
struct StatementList * add_to_fun_list(struct StatementList *s1, struct Statement *s2);
struct Statement * create_var_stmt(enum ExprType Type, struct Expression * expr);
struct StatementList * create_fun_list(struct Statement *s1);
struct Statement * create_sub_stmt(struct Expression * name ,struct StatementList *arg, struct StatementList *block);

char *root;
%}
%union {
int Int;
char *Id;
char *String;
char* str;
int ival;
struct Expression *Expr;
struct StatementList *SL;
struct Statement *Stmt;
};

%type <Expr> expr arg_fun mass mass_expr
%type <SL> program stmt_list if_list arg_list fun_list
%type <Stmt> stmt if_stmt alt_if while_stmt arg_stmt fun_stmt print_stmt scanf_stmt

%token <Int> INT
%token <Id> ID
%token <String> STRING_VB_CONST
%token ENDL
%token IF
%token THEN
%token END
%token ELSE
%token ELSEIF
%token WHILE
%token AS
%token DIM
%token INTEGER
%token STRING_VB
%token FUNCTION_VB
%token SUB
%token CONSOLE
%token READLINE
%token WRITELINE


%right '='
%left '<' '>'
%left '+' '-'
%left '*' '/'
%left UMINUS
%nonassoc ')'
%%

program : fun_list {root=$1; $$=$1;}
;

fun_list : fun_stmt {$$ = create_fun_list($1);}
| fun_list fun_stmt {$$ = add_to_fun_list($1, $2);}
;

stmt_list : stmt {$$=create_stmt_list($1);}
| stmt_list stmt {$$=add_to_stmt_list ($1, $2);}
;

stmt : expr ENDL {$$=create_simple_stmt(Expr,$1);}
| if_stmt {$$=$1;}
| while_stmt {$$=$1;}
| DIM expr AS INTEGER {$$ = create_var_stmt(Integer,$2);}
| DIM expr AS STRING_VB {$$ = create_var_stmt(String,$2);}
| print_stmt {$$=$1;}
| scanf_stmt {$$=$1;}
;

expr : INT {$$=create_const_expr($1);}
| ID {$$=create_id_expr($1);}
| STRING_VB_CONST {$$=create_string_expr($1);}
| expr '=' expr {$$=create_op_expr(As,$1,$3);}
| expr '+' expr {$$=create_op_expr(Plus,$1,$3);}
| expr '-' expr {$$=create_op_expr(Minus,$1,$3);}
| expr '*' expr {$$=create_op_expr(Mul,$1,$3);}
| expr '/' expr {$$=create_op_expr(Div,$1,$3);}
| expr '>' expr {$$=create_op_expr(GT,$1,$3);}
| expr '<' expr {$$=create_op_expr(LT,$1,$3);}
| FUNCTION_VB expr '(' expr ')' {$$=create_func_expr($2, NULL);}
| expr '(' ')' {$$=create_mass_expr($1, NULL);}
|'(' expr ')' {$$=$2;}
| expr ',' expr
| '-' expr %prec UMINUS {create_op_expr(UMinus,$2,0);}
| mass
;

mass : expr mass_expr {$$=create_mass_expr($1, $2);}
;

mass_expr : '(' INT ')' {$$=create_const_expr($2);}
;

if_stmt : IF expr THEN ENDL stmt_list ENDL END IF {$$=create_if_stmt($2,$5);}
| IF expr THEN ENDL if_list ENDL END IF {$$=create_if_stmt($2,$5);}
;

alt_if : ELSEIF expr THEN ENDL stmt_list {$$ = create_alt_if($2, $5);}
| ELSE stmt_list {$$ = create_alt_if(NULL, $2);}

if_list : if_list ENDL alt_if {$$ = add_to_if_list($1, $3);}
| alt_if {$$ = create_if_list($1);}

while_stmt: WHILE expr ENDL stmt_list ENDL WHILE END {$$ = create_while_stmt($2, $4)}

arg_fun : expr AS INTEGER {$$ = create_var_expr(Integer,$1);}
| expr AS STRING_VB {$$ = create_var_expr(String,$1);}
| arg_fun ',' arg_fun
;

arg_stmt: arg_fun {$$=create_simple_stmt(Expr,$1);}
;

arg_list: arg_list arg_stmt {$$ = add_to_stmt_list($1, $2);}
| arg_stmt {$$=create_stmt_list($1);}
;

print_stmt : CONSOLE '.' WRITELINE '(' stmt ')' {$$ = create_console(Write ,$5);}
;

scanf_stmt : CONSOLE '.' READLINE '(' stmt ')' {$$ = create_console(Read ,$5);}
;

fun_stmt: FUNCTION_VB expr '(' arg_list ')' AS INTEGER ENDL stmt_list ENDL END FUNCTION_VB ENDL {$$ = create_fun_stmt($2, $4, Integer, $9);}
| FUNCTION_VB expr '(' arg_list ')' AS STRING_VB ENDL stmt_list ENDL END FUNCTION_VB ENDL {$$ = create_fun_stmt($2, $4, String, $9);}
| fun_stmt ENDL
| SUB expr '(' arg_list ')' ENDL stmt_list ENDL END SUB ENDL {$$ = create_sub_stmt($2, $4, $7);}
| SUB expr '(' arg_list ')' ENDL stmt_list ENDL END SUB ENDL {$$ = create_sub_stmt($2, $4, $7);}
;
 
%%
int main(int argc, char *argv[])
{
	if(argc<=1)
	{
		puts("Error");
	}
	else
	{
		fopen_s(&yyin, argv[1], "r");
		yyparse();
	}
	_getch();
	return 0;
}

struct Expression * create_const_expr(int token)
{
struct Expression *Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Type = Int_const;
Result->Num = token;
return Result;
}

struct Expression * create_id_expr(char * token)
{
struct Expression *Result = (struct Expression *)malloc(sizeof(struct Expression));
Result->Type = Id;
Result->Name = token;
return Result;
}

struct Expression * create_string_expr(char * token)
{
struct Expression *Result = (struct Expression *)malloc(sizeof(struct Expression));
Result->Type = Str_const;
Result->Str = token;
return Result;
}

struct Expression * create_op_expr(enum ExprType Type, struct Expression *lhs, struct Expression *rhs)
{
struct Expression *Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Type = Type;
Result->Left = lhs;
Result->Right=rhs;
return Result;
}

struct Statement * create_simple_stmt(enum StmtType type, struct Expression *expr)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=type;
Result->Expr=expr;
return Result;
}

struct Statement * create_if_stmt(struct Expression *expr, struct StatementList *block)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=If;
Result->Expr=expr;
Result->Block=block;
Result->Next=NULL;
return Result;
}

struct StatementList * create_stmt_list(struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}

struct StatementList * add_to_stmt_list(struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}

struct StatementList * create_if_list (struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}

struct IfList * add_to_if_list (struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}

struct Statement * create_alt_if(struct Expression *expr, struct StatementList *block)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=AltIf;
Result->Expr=expr;
Result->Block=block;
Result->Next=NULL;
return Result;
}

struct Statement * create_while_stmt(struct Expression *expr, struct StatementList *block)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=While;
Result->Expr=expr;
Result->Block=block;
Result->Next=NULL;
return Result;
}

struct Statement * create_var_stmt(enum ExprType Type, struct Expression * expr)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Expr=expr;
Result->Expr->Type=Type;
return Result;
}

struct Expression * create_var_expr(enum ExprType Type, struct Expression * expr)
{
struct Expression * Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Left=expr;
Result->Type=Type;
return Result;
}

struct Statement * create_fun_stmt(struct Expression * name ,struct StatementList *arg, enum ExprType type,struct StatementList *block)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Expr = name;
Result->Arg = arg;
Result->Type = type;
Result->Block = block;
return Result;
}

struct Statement * create_sub_stmt(struct Expression * name ,struct StatementList *arg, struct StatementList *block)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Expr = name;
Result->Arg = arg;
Result->Block = block;
return Result;
}

struct Statement * create_console(enum StmtType type, struct Expression * e1)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Expr = e1;
Result->Type = type;
return Result;
}

struct Expression * create_func_expr(struct Expression * e1, struct Expression * e2)
{
struct Expression * Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Name=e1;
Result->Left=e2;
return Result;
}

struct Expression * create_mass_expr(char * token, int num)
{
struct Expression * Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Name=token;
Result->Num=num;
return Result;
}

struct StatementList * create_fun_list(struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}

struct StatementList * add_to_fun_list(struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}

void yyerror(char const *s)
{
 printf("%s",s);
}