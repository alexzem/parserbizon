typedef union {
	#include "tree_structs.h"
	void yyerror(char const *s);
    int int_value;
    char char_value;
    char* string_value;
    struct Expression* expr;
    struct Statement* stmt;
    struct List* list;
} YYSTYPE;
#define	INT	258
#define	CHAR	259
#define	STRING	260
#define	ID	261
#define	FALSE	262
#define	IF	263
#define	FROM	264
#define	IN	265
#define	THEN	266
#define	AS	267
#define	RETURN	268
#define	WHILE	269
#define	FOR	270
#define	TRUE	271
#define	BREAK	272
#define	ELIF	273
#define	ELSE	274
#define	DIM	275
#define	NEWLINE	276
#define	END_OF_FILE	277
#define	UMINUS	278


extern YYSTYPE yylval;
