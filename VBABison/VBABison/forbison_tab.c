
/*  A Bison parser, made from forbison.y with Bison version GNU Bison version 1.24
  */

#define YYBISON 1  /* Identify Bison output.  */

#define	INT	258
#define	ID	259
#define	STRING_VB_CONST	260
#define	ENDL	261
#define	IF	262
#define	THEN	263
#define	END	264
#define	ELSE	265
#define	ELSEIF	266
#define	WHILE	267
#define	AS	268
#define	DIM	269
#define	INTEGER	270
#define	STRING_VB	271
#define	FUNCTION_VB	272
#define	SUB	273
#define	CONSOLE	274
#define	READLINE	275
#define	WRITELINE	276
#define	UMINUS	277

#line 1 "forbison.y"

# include "stdafx.h"
#include "tree_structs_for_bison.h"
#include <stdio.h>
#include <malloc.h>
void yyerror(char const *s);
extern FILE *yyin;
extern int yylex(void);
struct Expression * create_const_expr(int token);
struct Expression * create_id_expr(char * token);
struct Expression * create_string_expr(char * token);
struct Expression * create_op_expr(enum ExprType Type, struct Expression *lhs, struct Expression *rhs);
struct Statement * create_simple_stmt(enum StmtType type, struct Expression *expr);
struct Statement * create_if_stmt(struct Expression *expr, struct StatementList *block);
struct StatementList * create_stmt_list(struct Statement *s1);
struct StatementList * add_to_stmt_list(struct StatementList *s1, struct StatementList *s2);
struct Statement * create_alt_if(struct Expression *expr, struct StatementList *block);
struct IfList * add_to_if_list (struct StatementList *s1, struct Statement *s2);
struct StatementList * create_if_list (struct Statement *s1);
struct Statement * create_while_stmt(struct Expression *expr, struct StatementList *block);
struct Expression * create_var_expr(enum ExprType Type, struct Expression * expr);
struct Statement * create_fun_stmt(struct Expression * name ,struct StatementList *arg, enum ExprType type,struct StatementList *block);
struct Statement * create_console(enum StmtType type, struct Expression * e1);
struct Expression * create_mass_expr(struct Expression * name, struct Expression * num);
struct Expression * create_func_expr(struct Expression * e1, struct Expression * e2);
struct Expression * create_mass_expr(char * token, int num);
struct StatementList * add_to_fun_list(struct StatementList *s1, struct Statement *s2);
struct Statement * create_var_stmt(enum ExprType Type, struct Expression * expr);
struct StatementList * create_fun_list(struct Statement *s1);
struct Statement * create_sub_stmt(struct Expression * name ,struct StatementList *arg, struct StatementList *block);

char *root;

#line 34 "forbison.y"
typedef union {
int Int;
char *Id;
char *String;
char* str;
int ival;
struct Expression *Expr;
struct StatementList *SL;
struct Statement *Stmt;
} YYSTYPE;

#ifndef YYLTYPE
typedef
  struct yyltype
    {
      int timestamp;
      int first_line;
      int first_column;
      int last_line;
      int last_column;
      char *text;
   }
  yyltype;

#define YYLTYPE yyltype
#endif

#include <stdio.h>

#ifndef __cplusplus
#ifndef __STDC__
#define const
#endif
#endif



#define	YYFINAL		134
#define	YYFLAG		-32768
#define	YYNTBASE	34

#define YYTRANSLATE(x) ((unsigned)(x) <= 277 ? yytranslate[x] : 51)

static const char yytranslate[] = {     0,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,    31,
    30,    27,    25,    32,    26,    33,    28,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,    23,
    22,    24,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
     2,     2,     2,     2,     2,     1,     2,     3,     4,     5,
     6,     7,     8,     9,    10,    11,    12,    13,    14,    15,
    16,    17,    18,    19,    20,    21,    29
};

#if YYDEBUG != 0
static const short yyprhs[] = {     0,
     0,     2,     4,     7,     9,    12,    15,    17,    19,    24,
    29,    31,    33,    35,    37,    39,    43,    47,    51,    55,
    59,    63,    67,    73,    77,    81,    85,    88,    90,    93,
    97,   106,   115,   121,   124,   128,   130,   138,   142,   146,
   150,   152,   155,   157,   164,   171,   185,   199,   202,   214
};

static const short yyrhs[] = {    35,
     0,    50,     0,    35,    50,     0,    37,     0,    36,    37,
     0,    38,     6,     0,    41,     0,    44,     0,    14,    38,
    13,    15,     0,    14,    38,    13,    16,     0,    48,     0,
    49,     0,     3,     0,     4,     0,     5,     0,    38,    22,
    38,     0,    38,    25,    38,     0,    38,    26,    38,     0,
    38,    27,    38,     0,    38,    28,    38,     0,    38,    24,
    38,     0,    38,    23,    38,     0,    17,    38,    31,    38,
    30,     0,    38,    31,    30,     0,    31,    38,    30,     0,
    38,    32,    38,     0,    26,    38,     0,    39,     0,    38,
    40,     0,    31,     3,    30,     0,     7,    38,     8,     6,
    36,     6,     9,     7,     0,     7,    38,     8,     6,    43,
     6,     9,     7,     0,    11,    38,     8,     6,    36,     0,
    10,    36,     0,    43,     6,    42,     0,    42,     0,    12,
    38,     6,    36,     6,    12,     9,     0,    38,    13,    15,
     0,    38,    13,    16,     0,    45,    32,    45,     0,    45,
     0,    47,    46,     0,    46,     0,    19,    33,    21,    31,
    37,    30,     0,    19,    33,    20,    31,    37,    30,     0,
    17,    38,    31,    47,    30,    13,    15,     6,    36,     6,
     9,    17,     6,     0,    17,    38,    31,    47,    30,    13,
    16,     6,    36,     6,     9,    17,     6,     0,    50,     6,
     0,    18,    38,    31,    47,    30,     6,    36,     6,     9,
    18,     6,     0,    18,    38,    31,    47,    30,     6,    36,
     6,     9,    18,     6,     0
};

#endif

#if YYDEBUG != 0
static const short yyrline[] = { 0,
    78,    81,    82,    85,    86,    89,    90,    91,    92,    93,
    94,    95,    98,    99,   100,   101,   102,   103,   104,   105,
   106,   107,   108,   109,   110,   111,   112,   113,   116,   119,
   122,   123,   126,   127,   129,   130,   132,   134,   135,   136,
   139,   142,   143,   146,   149,   152,   153,   154,   155,   156
};

static const char * const yytname[] = {   "$","error","$undefined.","INT","ID",
"STRING_VB_CONST","ENDL","IF","THEN","END","ELSE","ELSEIF","WHILE","AS","DIM",
"INTEGER","STRING_VB","FUNCTION_VB","SUB","CONSOLE","READLINE","WRITELINE","'='",
"'<'","'>'","'+'","'-'","'*'","'/'","UMINUS","')'","'('","','","'.'","program",
"fun_list","stmt_list","stmt","expr","mass","mass_expr","if_stmt","alt_if","if_list",
"while_stmt","arg_fun","arg_stmt","arg_list","print_stmt","scanf_stmt","fun_stmt",
""
};
#endif

static const short yyr1[] = {     0,
    34,    35,    35,    36,    36,    37,    37,    37,    37,    37,
    37,    37,    38,    38,    38,    38,    38,    38,    38,    38,
    38,    38,    38,    38,    38,    38,    38,    38,    39,    40,
    41,    41,    42,    42,    43,    43,    44,    45,    45,    45,
    46,    47,    47,    48,    49,    50,    50,    50,    50,    50
};

static const short yyr2[] = {     0,
     1,     1,     2,     1,     2,     2,     1,     1,     4,     4,
     1,     1,     1,     1,     1,     3,     3,     3,     3,     3,
     3,     3,     5,     3,     3,     3,     2,     1,     2,     3,
     8,     8,     5,     2,     3,     1,     7,     3,     3,     3,
     1,     2,     1,     6,     6,    13,    13,     2,    11,    11
};

static const short yydefact[] = {     0,
     0,     0,     1,     2,    13,    14,    15,     0,     0,     0,
     0,    28,     0,     3,    48,     0,    27,     0,     0,     0,
     0,     0,     0,     0,     0,     0,     0,    29,     0,     0,
     0,    25,    16,    22,    21,    17,    18,    19,    20,    13,
    24,     0,    41,    43,     0,    26,     0,     0,     0,    30,
     0,     0,     0,    42,     0,    23,    38,    39,    40,     0,
     0,     0,     0,     0,     0,     0,     0,     0,     4,     0,
     7,     8,    11,    12,     0,     0,     0,     0,     0,     0,
     0,     5,     6,     0,     0,     0,     0,     0,     0,     0,
     0,     0,     0,     0,     0,     9,    10,     0,     0,     0,
     0,     0,     0,     0,     0,    36,     0,     0,     0,     0,
    49,     0,     0,    34,     0,     0,     0,     0,    45,    44,
    46,    47,     0,     0,     0,    35,    37,     0,    31,    32,
    33,     0,     0,     0
};

static const short yydefgoto[] = {   132,
     3,    68,    69,    70,    12,    28,    71,   106,   107,    72,
    43,    44,    45,    73,    74,     4
};

static const short yypact[] = {    32,
   210,   210,    32,    -4,-32768,-32768,-32768,   210,   210,   210,
   318,-32768,   329,    -4,-32768,   340,   -15,   296,   210,   210,
   210,   210,   210,   210,   210,    10,   210,-32768,    10,    10,
     0,-32768,   351,     7,     7,    25,    25,   -15,   -15,   -19,
-32768,   269,     5,-32768,   192,   351,   203,   307,   -19,-32768,
    27,   210,    54,-32768,    39,-32768,-32768,-32768,     5,    44,
    67,    63,    72,   210,   210,   210,    47,   114,-32768,   220,
-32768,-32768,-32768,-32768,    67,    67,   242,   231,   285,    55,
    74,-32768,-32768,   132,   150,    79,    67,    90,    56,    60,
    82,    92,    99,    85,   168,-32768,-32768,    67,    67,   103,
    93,    95,    67,   210,   186,-32768,   107,   102,    94,    97,
-32768,   109,   116,    67,   253,   120,    -5,   121,-32768,-32768,
-32768,-32768,   117,   118,   125,-32768,-32768,    67,-32768,-32768,
    67,   134,   141,-32768
};

static const short yypgoto[] = {-32768,
-32768,   -21,   -37,    -1,-32768,-32768,-32768,    26,-32768,-32768,
    96,   -35,   113,-32768,-32768,   144
};


#define	YYLAST		383


static const short yytable[] = {    11,
    13,    15,    49,   125,   103,   104,    16,    17,    18,    54,
    50,    54,    40,     6,     7,    31,    27,    33,    34,    35,
    36,    37,    38,    39,    42,    46,     8,    42,    48,    41,
    82,    22,    23,    24,    25,     9,    52,    31,    27,    41,
    10,    57,    58,    42,    61,    42,    82,    82,     1,     2,
    42,    24,    25,    84,    85,    31,    27,    82,    62,    63,
   109,   110,    77,    78,    79,    95,    60,    82,    75,     5,
     6,     7,   105,    64,    89,    90,    82,    76,    65,    80,
    66,   114,    91,     8,    94,    67,    98,     5,     6,     7,
    99,    64,     9,    82,   103,   104,    65,    10,    66,   100,
   101,     8,   115,    67,    96,    97,   131,   102,   111,   112,
     9,   113,   117,   118,   121,    10,     5,     6,     7,    81,
    64,   122,   128,   119,   129,    65,   120,    66,   124,   127,
     8,   130,    67,   133,     5,     6,     7,    92,    64,     9,
   134,    47,   126,    65,    10,    66,    14,    59,     8,     0,
    67,     0,     5,     6,     7,    93,    64,     9,     0,     0,
     0,    65,    10,    66,     0,     0,     8,     0,    67,     0,
     5,     6,     7,   108,    64,     9,     0,     0,     0,    65,
    10,    66,     0,     0,     8,     0,    67,     0,     5,     6,
     7,   116,    64,     9,     5,     6,     7,    65,    10,    66,
     0,     0,     8,     0,    67,     5,     6,     7,     8,     0,
     0,     9,     5,     6,     7,     0,    10,     9,     0,     8,
     0,    53,    10,     0,     0,    83,     8,     0,     9,     0,
     0,     0,    55,    10,     0,     9,    87,     0,     0,     0,
    10,    19,    20,    21,    22,    23,    24,    25,     0,    86,
    31,    27,    19,    20,    21,    22,    23,    24,    25,     0,
   123,    31,    27,    19,    20,    21,    22,    23,    24,    25,
     0,     0,    31,    27,    19,    20,    21,    22,    23,    24,
    25,    51,     0,    31,    27,     0,     0,     0,     0,     0,
    19,    20,    21,    22,    23,    24,    25,    88,     0,    31,
    27,     0,     0,     0,     0,     0,    19,    20,    21,    22,
    23,    24,    25,     0,     0,    31,    27,    19,    20,    21,
    22,    23,    24,    25,     0,    32,    31,    27,    19,    20,
    21,    22,    23,    24,    25,     0,    56,    31,    27,    19,
    20,    21,    22,    23,    24,    25,     0,     0,    26,    27,
    19,    20,    21,    22,    23,    24,    25,     0,     0,    29,
    27,    19,    20,    21,    22,    23,    24,    25,     0,     0,
    30,    27,    19,    20,    21,    22,    23,    24,    25,     0,
     0,    31,    27
};

static const short yycheck[] = {     1,
     2,     6,     3,     9,    10,    11,     8,     9,    10,    45,
    30,    47,     3,     4,     5,    31,    32,    19,    20,    21,
    22,    23,    24,    25,    26,    27,    17,    29,    30,    30,
    68,    25,    26,    27,    28,    26,    32,    31,    32,    30,
    31,    15,    16,    45,     6,    47,    84,    85,    17,    18,
    52,    27,    28,    75,    76,    31,    32,    95,    15,    16,
    98,    99,    64,    65,    66,    87,    13,   105,     6,     3,
     4,     5,    94,     7,    20,    21,   114,     6,    12,    33,
    14,   103,     9,    17,     6,    19,    31,     3,     4,     5,
    31,     7,    26,   131,    10,    11,    12,    31,    14,    18,
     9,    17,   104,    19,    15,    16,   128,     9,     6,    17,
    26,    17,     6,    12,     6,    31,     3,     4,     5,     6,
     7,     6,     6,    30,     7,    12,    30,    14,     9,     9,
    17,     7,    19,     0,     3,     4,     5,     6,     7,    26,
     0,    29,   117,    12,    31,    14,     3,    52,    17,    -1,
    19,    -1,     3,     4,     5,     6,     7,    26,    -1,    -1,
    -1,    12,    31,    14,    -1,    -1,    17,    -1,    19,    -1,
     3,     4,     5,     6,     7,    26,    -1,    -1,    -1,    12,
    31,    14,    -1,    -1,    17,    -1,    19,    -1,     3,     4,
     5,     6,     7,    26,     3,     4,     5,    12,    31,    14,
    -1,    -1,    17,    -1,    19,     3,     4,     5,    17,    -1,
    -1,    26,     3,     4,     5,    -1,    31,    26,    -1,    17,
    -1,    30,    31,    -1,    -1,     6,    17,    -1,    26,    -1,
    -1,    -1,    30,    31,    -1,    26,     6,    -1,    -1,    -1,
    31,    22,    23,    24,    25,    26,    27,    28,    -1,     8,
    31,    32,    22,    23,    24,    25,    26,    27,    28,    -1,
     8,    31,    32,    22,    23,    24,    25,    26,    27,    28,
    -1,    -1,    31,    32,    22,    23,    24,    25,    26,    27,
    28,    13,    -1,    31,    32,    -1,    -1,    -1,    -1,    -1,
    22,    23,    24,    25,    26,    27,    28,    13,    -1,    31,
    32,    -1,    -1,    -1,    -1,    -1,    22,    23,    24,    25,
    26,    27,    28,    -1,    -1,    31,    32,    22,    23,    24,
    25,    26,    27,    28,    -1,    30,    31,    32,    22,    23,
    24,    25,    26,    27,    28,    -1,    30,    31,    32,    22,
    23,    24,    25,    26,    27,    28,    -1,    -1,    31,    32,
    22,    23,    24,    25,    26,    27,    28,    -1,    -1,    31,
    32,    22,    23,    24,    25,    26,    27,    28,    -1,    -1,
    31,    32,    22,    23,    24,    25,    26,    27,    28,    -1,
    -1,    31,    32
};
/* -*-C-*-  Note some compilers choke on comments on `#line' lines.  */
#line 3 "bison.simple"

/* Skeleton output parser for bison,
   Copyright (C) 1984, 1989, 1990 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

#ifndef alloca
#ifdef __GNUC__
#define alloca __builtin_alloca
#else /* not GNU C.  */
#if (!defined (__STDC__) && defined (sparc)) || defined (__sparc__) || defined (__sparc) || defined (__sgi)
#include <alloca.h>
#else /* not sparc */
#if defined (MSDOS) && !defined (__TURBOC__)
#include <malloc.h>
#else /* not MSDOS, or __TURBOC__ */
#if defined(_AIX)
#include <malloc.h>
 #pragma alloca
#else /* not MSDOS, __TURBOC__, or _AIX */
#ifdef __hpux
#ifdef __cplusplus
extern "C" {
void *alloca (unsigned int);
};
#else /* not __cplusplus */
void *alloca ();
#endif /* not __cplusplus */
#endif /* __hpux */
#endif /* not _AIX */
#endif /* not MSDOS, or __TURBOC__ */
#endif /* not sparc.  */
#endif /* not GNU C.  */
#endif /* alloca not defined.  */

/* This is the parser code that is written into each bison parser
  when the %semantic_parser declaration is not specified in the grammar.
  It was written by Richard Stallman by simplifying the hairy parser
  used when %semantic_parser is specified.  */

/* Note: there must be only one dollar sign in this file.
   It is replaced by the list of actions, each action
   as one case of the switch.  */

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		-2
#define YYEOF		0
#define YYACCEPT	return(0)
#define YYABORT 	return(1)
#define YYERROR		goto yyerrlab1
/* Like YYERROR except do call yyerror.
   This remains here temporarily to ease the
   transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */
#define YYFAIL		goto yyerrlab
#define YYRECOVERING()  (!!yyerrstatus)
#define YYBACKUP(token, value) \
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    { yychar = (token), yylval = (value);			\
      yychar1 = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { yyerror ("syntax error: cannot back up"); YYERROR; }	\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

#ifndef YYPURE
#define YYLEX		yylex()
#endif

#ifdef YYPURE
#ifdef YYLSP_NEEDED
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, &yylloc, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval, &yylloc)
#endif
#else /* not YYLSP_NEEDED */
#ifdef YYLEX_PARAM
#define YYLEX		yylex(&yylval, YYLEX_PARAM)
#else
#define YYLEX		yylex(&yylval)
#endif
#endif /* not YYLSP_NEEDED */
#endif

/* If nonreentrant, generate the variables here */

#ifndef YYPURE

int	yychar;			/*  the lookahead symbol		*/
YYSTYPE	yylval;			/*  the semantic value of the		*/
				/*  lookahead symbol			*/

#ifdef YYLSP_NEEDED
YYLTYPE yylloc;			/*  location data for the lookahead	*/
				/*  symbol				*/
#endif

int yynerrs;			/*  number of parse errors so far       */
#endif  /* not YYPURE */

#if YYDEBUG != 0
int yydebug;			/*  nonzero means print parse trace	*/
/* Since this is uninitialized, it does not stop multiple parsers
   from coexisting.  */
#endif

/*  YYINITDEPTH indicates the initial size of the parser's stacks	*/

#ifndef	YYINITDEPTH
#define YYINITDEPTH 200
#endif

/*  YYMAXDEPTH is the maximum size the stacks can grow to
    (effective only if the built-in stack extension method is used).  */

#if YYMAXDEPTH == 0
#undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
#define YYMAXDEPTH 10000
#endif

/* Prevent warning if -Wstrict-prototypes.  */
#ifdef __GNUC__
int yyparse (void);
#endif

#if __GNUC__ > 1		/* GNU C and GNU C++ define this.  */
#define __yy_memcpy(FROM,TO,COUNT)	__builtin_memcpy(TO,FROM,COUNT)
#else				/* not GNU C or C++ */
#ifndef __cplusplus

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (from, to, count)
     char *from;
     char *to;
     int count;
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#else /* __cplusplus */

/* This is the most reliable way to avoid incompatibilities
   in available built-in functions on various systems.  */
static void
__yy_memcpy (char *from, char *to, int count)
{
  register char *f = from;
  register char *t = to;
  register int i = count;

  while (i-- > 0)
    *t++ = *f++;
}

#endif
#endif

#line 192 "bison.simple"

/* The user can define YYPARSE_PARAM as the name of an argument to be passed
   into yyparse.  The argument should have type void *.
   It should actually point to an object.
   Grammar actions can access the variable by casting it
   to the proper pointer type.  */

#ifdef YYPARSE_PARAM
#define YYPARSE_PARAM_DECL void *YYPARSE_PARAM;
#else
#define YYPARSE_PARAM
#define YYPARSE_PARAM_DECL
#endif

int
yyparse(YYPARSE_PARAM)
     YYPARSE_PARAM_DECL
{
  register int yystate;
  register int yyn;
  register short *yyssp;
  register YYSTYPE *yyvsp;
  int yyerrstatus;	/*  number of tokens to shift before error messages enabled */
  int yychar1 = 0;		/*  lookahead token as an internal (translated) token number */

  short	yyssa[YYINITDEPTH];	/*  the state stack			*/
  YYSTYPE yyvsa[YYINITDEPTH];	/*  the semantic value stack		*/

  short *yyss = yyssa;		/*  refer to the stacks thru separate pointers */
  YYSTYPE *yyvs = yyvsa;	/*  to allow yyoverflow to reallocate them elsewhere */

#ifdef YYLSP_NEEDED
  YYLTYPE yylsa[YYINITDEPTH];	/*  the location stack			*/
  YYLTYPE *yyls = yylsa;
  YYLTYPE *yylsp;

#define YYPOPSTACK   (yyvsp--, yyssp--, yylsp--)
#else
#define YYPOPSTACK   (yyvsp--, yyssp--)
#endif

  int yystacksize = YYINITDEPTH;

#ifdef YYPURE
  int yychar;
  YYSTYPE yylval;
  int yynerrs;
#ifdef YYLSP_NEEDED
  YYLTYPE yylloc;
#endif
#endif

  YYSTYPE yyval;		/*  the variable used to return		*/
				/*  semantic values from the action	*/
				/*  routines				*/

  int yylen;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Starting parse\n");
#endif

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss - 1;
  yyvsp = yyvs;
#ifdef YYLSP_NEEDED
  yylsp = yyls;
#endif

/* Push a new state, which is found in  yystate  .  */
/* In all cases, when you get here, the value and location stacks
   have just been pushed. so pushing a state here evens the stacks.  */
yynewstate:

  *++yyssp = yystate;

  if (yyssp >= yyss + yystacksize - 1)
    {
      /* Give user a chance to reallocate the stack */
      /* Use copies of these so that the &'s don't force the real ones into memory. */
      YYSTYPE *yyvs1 = yyvs;
      short *yyss1 = yyss;
#ifdef YYLSP_NEEDED
      YYLTYPE *yyls1 = yyls;
#endif

      /* Get the current used size of the three stacks, in elements.  */
      int size = yyssp - yyss + 1;

#ifdef yyoverflow
      /* Each stack pointer address is followed by the size of
	 the data in use in that stack, in bytes.  */
#ifdef YYLSP_NEEDED
      /* This used to be a conditional around just the two extra args,
	 but that might be undefined if yyoverflow is a macro.  */
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yyls1, size * sizeof (*yylsp),
		 &yystacksize);
#else
      yyoverflow("parser stack overflow",
		 &yyss1, size * sizeof (*yyssp),
		 &yyvs1, size * sizeof (*yyvsp),
		 &yystacksize);
#endif

      yyss = yyss1; yyvs = yyvs1;
#ifdef YYLSP_NEEDED
      yyls = yyls1;
#endif
#else /* no yyoverflow */
      /* Extend the stack our own way.  */
      if (yystacksize >= YYMAXDEPTH)
	{
	  yyerror("parser stack overflow");
	  return 2;
	}
      yystacksize *= 2;
      if (yystacksize > YYMAXDEPTH)
	yystacksize = YYMAXDEPTH;
      yyss = (short *) alloca (yystacksize * sizeof (*yyssp));
      __yy_memcpy ((char *)yyss1, (char *)yyss, size * sizeof (*yyssp));
      yyvs = (YYSTYPE *) alloca (yystacksize * sizeof (*yyvsp));
      __yy_memcpy ((char *)yyvs1, (char *)yyvs, size * sizeof (*yyvsp));
#ifdef YYLSP_NEEDED
      yyls = (YYLTYPE *) alloca (yystacksize * sizeof (*yylsp));
      __yy_memcpy ((char *)yyls1, (char *)yyls, size * sizeof (*yylsp));
#endif
#endif /* no yyoverflow */

      yyssp = yyss + size - 1;
      yyvsp = yyvs + size - 1;
#ifdef YYLSP_NEEDED
      yylsp = yyls + size - 1;
#endif

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Stack size increased to %d\n", yystacksize);
#endif

      if (yyssp >= yyss + yystacksize - 1)
	YYABORT;
    }

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Entering state %d\n", yystate);
#endif

  goto yybackup;
 yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* yychar is either YYEMPTY or YYEOF
     or a valid token in external form.  */

  if (yychar == YYEMPTY)
    {
#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Reading a token: ");
#endif
      yychar = YYLEX;
    }

  /* Convert token to internal form (in yychar1) for indexing tables with */

  if (yychar <= 0)		/* This means end of input. */
    {
      yychar1 = 0;
      yychar = YYEOF;		/* Don't call YYLEX any more */

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Now at end of input.\n");
#endif
    }
  else
    {
      yychar1 = YYTRANSLATE(yychar);

#if YYDEBUG != 0
      if (yydebug)
	{
	  fprintf (stderr, "Next token is %d (%s", yychar, yytname[yychar1]);
	  /* Give the individual parser a way to print the precise meaning
	     of a token, for further debugging info.  */
#ifdef YYPRINT
	  YYPRINT (stderr, yychar, yylval);
#endif
	  fprintf (stderr, ")\n");
	}
#endif
    }

  yyn += yychar1;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != yychar1)
    goto yydefault;

  yyn = yytable[yyn];

  /* yyn is what to do for this token type in this state.
     Negative => reduce, -yyn is rule number.
     Positive => shift, yyn is new state.
       New state is final state => don't bother to shift,
       just return success.
     0, or most negative number => error.  */

  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrlab;

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting token %d (%s), ", yychar, yytname[yychar1]);
#endif

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  /* count tokens shifted since error; after three, turn off error status.  */
  if (yyerrstatus) yyerrstatus--;

  yystate = yyn;
  goto yynewstate;

/* Do the default action for the current state.  */
yydefault:

  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;

/* Do a reduction.  yyn is the number of a rule to reduce with.  */
yyreduce:
  yylen = yyr2[yyn];
  if (yylen > 0)
    yyval = yyvsp[1-yylen]; /* implement default value of the action */

#if YYDEBUG != 0
  if (yydebug)
    {
      int i;

      fprintf (stderr, "Reducing via rule %d (line %d), ",
	       yyn, yyrline[yyn]);

      /* Print the symbols being reduced, and their result.  */
      for (i = yyprhs[yyn]; yyrhs[i] > 0; i++)
	fprintf (stderr, "%s ", yytname[yyrhs[i]]);
      fprintf (stderr, " -> %s\n", yytname[yyr1[yyn]]);
    }
#endif


  switch (yyn) {

case 1:
#line 78 "forbison.y"
{root=yyvsp[0].SL; yyval.SL=yyvsp[0].SL;;
    break;}
case 2:
#line 81 "forbison.y"
{yyval.SL = create_fun_list(yyvsp[0].Stmt);;
    break;}
case 3:
#line 82 "forbison.y"
{yyval.SL = add_to_fun_list(yyvsp[-1].SL, yyvsp[0].Stmt);;
    break;}
case 4:
#line 85 "forbison.y"
{yyval.SL=create_stmt_list(yyvsp[0].Stmt);;
    break;}
case 5:
#line 86 "forbison.y"
{yyval.SL=add_to_stmt_list (yyvsp[-1].SL, yyvsp[0].Stmt);;
    break;}
case 6:
#line 89 "forbison.y"
{yyval.Stmt=create_simple_stmt(Expr,yyvsp[-1].Expr);;
    break;}
case 7:
#line 90 "forbison.y"
{yyval.Stmt=yyvsp[0].Stmt;;
    break;}
case 8:
#line 91 "forbison.y"
{yyval.Stmt=yyvsp[0].Stmt;;
    break;}
case 9:
#line 92 "forbison.y"
{yyval.Stmt = create_var_stmt(Integer,yyvsp[-2].Expr);;
    break;}
case 10:
#line 93 "forbison.y"
{yyval.Stmt = create_var_stmt(String,yyvsp[-2].Expr);;
    break;}
case 11:
#line 94 "forbison.y"
{yyval.Stmt=yyvsp[0].Stmt;;
    break;}
case 12:
#line 95 "forbison.y"
{yyval.Stmt=yyvsp[0].Stmt;;
    break;}
case 13:
#line 98 "forbison.y"
{yyval.Expr=create_const_expr(yyvsp[0].Int);;
    break;}
case 14:
#line 99 "forbison.y"
{yyval.Expr=create_id_expr(yyvsp[0].Id);;
    break;}
case 15:
#line 100 "forbison.y"
{yyval.Expr=create_string_expr(yyvsp[0].String);;
    break;}
case 16:
#line 101 "forbison.y"
{yyval.Expr=create_op_expr(As,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 17:
#line 102 "forbison.y"
{yyval.Expr=create_op_expr(Plus,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 18:
#line 103 "forbison.y"
{yyval.Expr=create_op_expr(Minus,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 19:
#line 104 "forbison.y"
{yyval.Expr=create_op_expr(Mul,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 20:
#line 105 "forbison.y"
{yyval.Expr=create_op_expr(Div,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 21:
#line 106 "forbison.y"
{yyval.Expr=create_op_expr(GT,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 22:
#line 107 "forbison.y"
{yyval.Expr=create_op_expr(LT,yyvsp[-2].Expr,yyvsp[0].Expr);;
    break;}
case 23:
#line 108 "forbison.y"
{yyval.Expr=create_func_expr(yyvsp[-3].Expr, NULL);;
    break;}
case 24:
#line 109 "forbison.y"
{yyval.Expr=create_mass_expr(yyvsp[-2].Expr, NULL);;
    break;}
case 25:
#line 110 "forbison.y"
{yyval.Expr=yyvsp[-1].Expr;;
    break;}
case 27:
#line 112 "forbison.y"
{create_op_expr(UMinus,yyvsp[0].Expr,0);;
    break;}
case 29:
#line 116 "forbison.y"
{yyval.Expr=create_mass_expr(yyvsp[-1].Expr, yyvsp[0].Expr);;
    break;}
case 30:
#line 119 "forbison.y"
{yyval.Expr=create_const_expr(yyvsp[-1].Int);;
    break;}
case 31:
#line 122 "forbison.y"
{yyval.Stmt=create_if_stmt(yyvsp[-6].Expr,yyvsp[-3].SL);;
    break;}
case 32:
#line 123 "forbison.y"
{yyval.Stmt=create_if_stmt(yyvsp[-6].Expr,yyvsp[-3].SL);;
    break;}
case 33:
#line 126 "forbison.y"
{yyval.Stmt = create_alt_if(yyvsp[-3].Expr, yyvsp[0].SL);;
    break;}
case 34:
#line 127 "forbison.y"
{yyval.Stmt = create_alt_if(NULL, yyvsp[0].SL);;
    break;}
case 35:
#line 129 "forbison.y"
{yyval.SL = add_to_if_list(yyvsp[-2].SL, yyvsp[0].Stmt);;
    break;}
case 36:
#line 130 "forbison.y"
{yyval.SL = create_if_list(yyvsp[0].Stmt);;
    break;}
case 37:
#line 132 "forbison.y"
{yyval.Stmt = create_while_stmt(yyvsp[-5].Expr, yyvsp[-3].SL);
    break;}
case 38:
#line 134 "forbison.y"
{yyval.Expr = create_var_expr(Integer,yyvsp[-2].Expr);;
    break;}
case 39:
#line 135 "forbison.y"
{yyval.Expr = create_var_expr(String,yyvsp[-2].Expr);;
    break;}
case 41:
#line 139 "forbison.y"
{yyval.Stmt=create_simple_stmt(Expr,yyvsp[0].Expr);;
    break;}
case 42:
#line 142 "forbison.y"
{yyval.SL = add_to_stmt_list(yyvsp[-1].SL, yyvsp[0].Stmt);;
    break;}
case 43:
#line 143 "forbison.y"
{yyval.SL=create_stmt_list(yyvsp[0].Stmt);;
    break;}
case 44:
#line 146 "forbison.y"
{yyval.Stmt = create_console(Write ,yyvsp[-1].Stmt);;
    break;}
case 45:
#line 149 "forbison.y"
{yyval.Stmt = create_console(Read ,yyvsp[-1].Stmt);;
    break;}
case 46:
#line 152 "forbison.y"
{yyval.Stmt = create_fun_stmt(yyvsp[-11].Expr, yyvsp[-9].SL, Integer, yyvsp[-4].SL);;
    break;}
case 47:
#line 153 "forbison.y"
{yyval.Stmt = create_fun_stmt(yyvsp[-11].Expr, yyvsp[-9].SL, String, yyvsp[-4].SL);;
    break;}
case 49:
#line 155 "forbison.y"
{yyval.Stmt = create_sub_stmt(yyvsp[-9].Expr, yyvsp[-7].SL, yyvsp[-4].SL);;
    break;}
case 50:
#line 156 "forbison.y"
{yyval.Stmt = create_sub_stmt(yyvsp[-9].Expr, yyvsp[-7].SL, yyvsp[-4].SL);;
    break;}
}
   /* the action file gets copied in in place of this dollarsign */
#line 487 "bison.simple"

  yyvsp -= yylen;
  yyssp -= yylen;
#ifdef YYLSP_NEEDED
  yylsp -= yylen;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

  *++yyvsp = yyval;

#ifdef YYLSP_NEEDED
  yylsp++;
  if (yylen == 0)
    {
      yylsp->first_line = yylloc.first_line;
      yylsp->first_column = yylloc.first_column;
      yylsp->last_line = (yylsp-1)->last_line;
      yylsp->last_column = (yylsp-1)->last_column;
      yylsp->text = 0;
    }
  else
    {
      yylsp->last_line = (yylsp+yylen-1)->last_line;
      yylsp->last_column = (yylsp+yylen-1)->last_column;
    }
#endif

  /* Now "shift" the result of the reduction.
     Determine what state that goes to,
     based on the state we popped back to
     and the rule number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTBASE] + *yyssp;
  if (yystate >= 0 && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTBASE];

  goto yynewstate;

yyerrlab:   /* here on detecting error */

  if (! yyerrstatus)
    /* If not already recovering from an error, report this error.  */
    {
      ++yynerrs;

#ifdef YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (yyn > YYFLAG && yyn < YYLAST)
	{
	  int size = 0;
	  char *msg;
	  int x, count;

	  count = 0;
	  /* Start X at -yyn if nec to avoid negative indexes in yycheck.  */
	  for (x = (yyn < 0 ? -yyn : 0);
	       x < (sizeof(yytname) / sizeof(char *)); x++)
	    if (yycheck[x + yyn] == x)
	      size += strlen(yytname[x]) + 15, count++;
	  msg = (char *) malloc(size + 15);
	  if (msg != 0)
	    {
	      strcpy(msg, "parse error");

	      if (count < 5)
		{
		  count = 0;
		  for (x = (yyn < 0 ? -yyn : 0);
		       x < (sizeof(yytname) / sizeof(char *)); x++)
		    if (yycheck[x + yyn] == x)
		      {
			strcat(msg, count == 0 ? ", expecting `" : " or `");
			strcat(msg, yytname[x]);
			strcat(msg, "'");
			count++;
		      }
		}
	      yyerror(msg);
	      free(msg);
	    }
	  else
	    yyerror ("parse error; also virtual memory exceeded");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror("parse error");
    }

  goto yyerrlab1;
yyerrlab1:   /* here on error raised explicitly by an action */

  if (yyerrstatus == 3)
    {
      /* if just tried and failed to reuse lookahead token after an error, discard it.  */

      /* return failure if at end of input */
      if (yychar == YYEOF)
	YYABORT;

#if YYDEBUG != 0
      if (yydebug)
	fprintf(stderr, "Discarding token %d (%s).\n", yychar, yytname[yychar1]);
#endif

      yychar = YYEMPTY;
    }

  /* Else will try to reuse lookahead token
     after shifting the error token.  */

  yyerrstatus = 3;		/* Each real token shifted decrements this */

  goto yyerrhandle;

yyerrdefault:  /* current state does not do anything special for the error token. */

#if 0
  /* This is wrong; only states that explicitly want error tokens
     should shift them.  */
  yyn = yydefact[yystate];  /* If its default is to accept any token, ok.  Otherwise pop it.*/
  if (yyn) goto yydefault;
#endif

yyerrpop:   /* pop the current state because it cannot handle the error token */

  if (yyssp == yyss) YYABORT;
  yyvsp--;
  yystate = *--yyssp;
#ifdef YYLSP_NEEDED
  yylsp--;
#endif

#if YYDEBUG != 0
  if (yydebug)
    {
      short *ssp1 = yyss - 1;
      fprintf (stderr, "Error: state stack now");
      while (ssp1 != yyssp)
	fprintf (stderr, " %d", *++ssp1);
      fprintf (stderr, "\n");
    }
#endif

yyerrhandle:

  yyn = yypact[yystate];
  if (yyn == YYFLAG)
    goto yyerrdefault;

  yyn += YYTERROR;
  if (yyn < 0 || yyn > YYLAST || yycheck[yyn] != YYTERROR)
    goto yyerrdefault;

  yyn = yytable[yyn];
  if (yyn < 0)
    {
      if (yyn == YYFLAG)
	goto yyerrpop;
      yyn = -yyn;
      goto yyreduce;
    }
  else if (yyn == 0)
    goto yyerrpop;

  if (yyn == YYFINAL)
    YYACCEPT;

#if YYDEBUG != 0
  if (yydebug)
    fprintf(stderr, "Shifting error token, ");
#endif

  *++yyvsp = yylval;
#ifdef YYLSP_NEEDED
  *++yylsp = yylloc;
#endif

  yystate = yyn;
  goto yynewstate;
}
#line 159 "forbison.y"

int main(int argc, char *argv[])
{
	if(argc<=1)
	{
		puts("Error");
	}
	else
	{
		fopen_s(&yyin, argv[1], "r");
		yyparse();
	}
	_getch();
	return 0;
}

struct Expression * create_const_expr(int token)
{
struct Expression *Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Type = Int_const;
Result->Num = token;
return Result;
}

struct Expression * create_id_expr(char * token)
{
struct Expression *Result = (struct Expression *)malloc(sizeof(struct Expression));
Result->Type = Id;
Result->Name = token;
return Result;
}

struct Expression * create_string_expr(char * token)
{
struct Expression *Result = (struct Expression *)malloc(sizeof(struct Expression));
Result->Type = Str_const;
Result->Str = token;
return Result;
}

struct Expression * create_op_expr(enum ExprType Type, struct Expression *lhs, struct Expression *rhs)
{
struct Expression *Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Type = Type;
Result->Left = lhs;
Result->Right=rhs;
return Result;
}

struct Statement * create_simple_stmt(enum StmtType type, struct Expression *expr)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=type;
Result->Expr=expr;
return Result;
}

struct Statement * create_if_stmt(struct Expression *expr, struct StatementList *block)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=If;
Result->Expr=expr;
Result->Block=block;
Result->Next=NULL;
return Result;
}

struct StatementList * create_stmt_list(struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}

struct StatementList * add_to_stmt_list(struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}

struct StatementList * create_if_list (struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}

struct IfList * add_to_if_list (struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}

struct Statement * create_alt_if(struct Expression *expr, struct StatementList *block)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=AltIf;
Result->Expr=expr;
Result->Block=block;
Result->Next=NULL;
return Result;
}

struct Statement * create_while_stmt(struct Expression *expr, struct StatementList *block)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=While;
Result->Expr=expr;
Result->Block=block;
Result->Next=NULL;
return Result;
}

struct Statement * create_var_stmt(enum ExprType Type, struct Expression * expr)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Expr=expr;
Result->Expr->Type=Type;
return Result;
}

struct Expression * create_var_expr(enum ExprType Type, struct Expression * expr)
{
struct Expression * Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Left=expr;
Result->Type=Type;
return Result;
}

struct Statement * create_fun_stmt(struct Expression * name ,struct StatementList *arg, enum ExprType type,struct StatementList *block)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Expr = name;
Result->Arg = arg;
Result->Type = type;
Result->Block = block;
return Result;
}

struct Statement * create_sub_stmt(struct Expression * name ,struct StatementList *arg, struct StatementList *block)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Expr = name;
Result->Arg = arg;
Result->Block = block;
return Result;
}

struct Statement * create_console(enum StmtType type, struct Expression * e1)
{
struct Statement * Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Expr = e1;
Result->Type = type;
return Result;
}

struct Expression * create_func_expr(struct Expression * e1, struct Expression * e2)
{
struct Expression * Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Name=e1;
Result->Left=e2;
return Result;
}

struct Expression * create_mass_expr(char * token, int num)
{
struct Expression * Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Name=token;
Result->Num=num;
return Result;
}

struct StatementList * create_fun_list(struct Statement *s1)
{
struct StatementList *Result = (struct StatementList *) malloc(sizeof(struct StatementList));
Result->First=s1;
Result->Last=s1;
return Result;
}

struct StatementList * add_to_fun_list(struct StatementList *s1, struct Statement *s2)
{
s1->Last->Next=s2;
s1->Last=s2;
return s1;
}

void yyerror(char const *s)
{
 printf("%s",s);
}