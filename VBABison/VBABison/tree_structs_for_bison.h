#ifndef SIMPLE_TREE_STRUCTS
#define SIMPLE_TREE_STRUCTS
enum ExprType {Int_const, Id, Plus, Minus, Mul, Div, As, LT, GT, Prior, UMinus, Str_const, Integer, String};
enum StmtType {Expr, If, Read, AltIf, While, Write};

struct Expression {
enum ExprType Type;
int Num;
char *Name;
char *Str;
struct Expression *Left;
struct Expression *Right;
};

struct StatementList {
struct Statement *First;
struct Statement *Last;
};

struct Statement {
enum StmtType Type;
struct Expression *Expr;
struct StatementList *Block;
struct StatementList *Arg;
struct Statement *Next;
};

struct AltIf {
	struct Expression *Expr;
	struct StatementList *Block;
};

struct IfList {
	struct AltIf *First;
	struct AltIF *Last;
};

struct Function {
	enum ExprType Type;
	struct Expression *Name;
	struct StatementList *Arg;
	struct StatementList *Block;
};
#endif
