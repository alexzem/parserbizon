%option noyywrap
%option never-interactive
%option yylineno
%option caseless

%{
#include "tree_structs_for_bison.h"
#include "forbison_tab.h"
#include <stdio.h>
#include <conio.h>
#include <stdlib.h>
#include <math.h>
extern int yyparse(void);
%}

%x STRING

NUM [0-9]+
ID [a-zA-Z]([_a-zA-Z0-9]{1,254})?

%%

%{
    char comment[1000];
	comment[0] = 0;
%}

\n {return ENDL;}

\" {BEGIN(STRING); comment[0] = 0;}
<STRING>\n		{printf("(%d:0) String constants must end with a double quote.\n",yylineno-1);BEGIN(INITIAL);}
<STRING>\"\"			strcat(comment,"\"");
<STRING>[^\n""]+		strcat(comment,yytext);
<STRING>\" {BEGIN(INITIAL);
yylval.str = (char *)malloc(strlen(yytext)+1); 
strcpy(yylval.str, comment);
return STRING_VB_CONST;}

\= {return '=';}
\* {return '*';}
\/ {return '/';}
\+ {return '+';}
- {return '-';}
>  {return '>';}
\< {return '<';}
, {return ',';}
\( {return '(';}
\) {return ')';}

If {return IF;}
Then {return THEN;}
ElseIf {return ELSEIF;}
Else {return ELSE;}
End {return END;}
While {return WHILE;}
Dim {return DIM;}
As {return AS;}
Function {return FUNCTION_VB;}
Sub {return SUB;}
Writeline {return WRITELINE;}
Readline {return READLINE;}
Console {return CONSOLE;}

Integer {return INTEGER;}
String {return STRING_VB;}

{NUM} {yylval.ival=atoi(yytext);
return INT;}

{ID} {yylval.str=(char *)malloc(strlen(yytext)+1);
strcpy(yylval.str, yytext);
return ID;}

%%