#ifndef SIMPLE_TREE_STRUCTS
#define SIMPLE_TREE_STRUCTS

enum ExprType {Int, Id, Plus, Minus, Mul, Div,
As, LT, GT, EQ, NE, Prior, UMinus};
enum StmtType {Expr,If,Print};
struct Expression
{
	enum ExprType Type;//0 - int, 1 +, 2-, 3*, 4/
	int value;
	char *Name;
	struct Expression *left;
	struct Expression *right;
};

struct Statement
{
	enum StmtType Type;
	struct Expression *Expr;
	struct StatementList *Block;
	struct Statement *Next;
};

struct Program
{
	struct Statements_List *stmts;
};

struct Statements_List
{
	struct Statement *stm;
	struct Statements_List *next;
};
// Создать выражение
struct Expression * createExpression
(enum ExprType Type,
struct Expression *lhs,
struct Expression *rhs)
{
	struct Expression *Result =(struct Expression *) malloc(sizeof(struct Expression));
	Result->Type = Type;
	Result->left=lhs;
	Result->right=rhs;
	return Result;
}

struct Statement * createReturnStatement
(enum ExprType Type,
struct Expression *returnRst)
{
	struct Statement *Result =(struct Statement*) malloc(sizeof(struct Statement));
	Result->Type = Print;
	Result->Expr = returnRst;
	return Result
}

struct Expression * createConstExpr(int token)
{
struct Expression *Result =(struct Expression *) malloc(sizeof(struct Expression));
Result->Type = Int;
Result->value = token;
return Result;
}

struct Expression * createIdExpr(char * token)
{
struct Expression *Result = (struct Expression *) malloc(sizeof(struct Expression));
Result->Type = Id;
Result->Name = token;
return Result;
}

struct Statement * createIfStmt
(struct Expression *expr,
struct StatementList *block)
{
struct Statement *Result =(struct Statement *) malloc(sizeof(struct Statement));
Result->Type=If;
Result->Expr=expr;
Result->Block=block;
Result->Next=NULL
return Result;
#endif