%union {
	#include "tree_structs.h"
	void yyerror(char const *s);
    int int_value;
    char char_value;
    char* string_value;
    struct Expression* expr;
    struct Statement* stmt;
    struct List* list;
}
%type <expr> expression

%token <int_value> INT
%token <char_value> CHAR
%token <string_value> STRING
%token <string_value> ID

%token FALSE
%token IF
%token FROM
%token IN
%token THEN
%token AS
%token RETURN
%token WHILE
%token FOR
%token TRUE
%token BREAK
%token ELIF
%token ELSE
%token DIM

%token NEWLINE

%token END_OF_FILE

%right '=' 
%left '+' '-'
%left '*' '/' '%'
%left UMINUS
%nonassoc ')'
%nonassoc ']'
%nonassoc '}'

%% 
expression  : | expression '+' expression   								 { $$ = createExpression(ET_PLUS, $1, $3); }
   		 | expression '-' expression   								 { $$ = createExpression(ET_MINUS, $1, $3); }
   		 | expression '*' expression   								 { $$ = createExpression(ET_MULT, $1, $3); }
   		 | expression '/' expression   								 { $$ = createExpression(ET_DIV, $1, $3); }

%%
void yyerror(char const *s)
{
 printf("%s",s);
}