#ifndef SIMPLE_TREE_STRUCTS
#define SIMPLE_TREE_STRUCTS

enum ExprType {Int, Id, Plus, Minus, Mul, Div,
As, LT, GT, EQ, NE, Prior, UMinus};
enum StmtType {Expr,If,Print};
struct Expression
{
	enum ExprType Type;//0 - int, 1 +, 2-, 3*, 4/
	int value;
	char *Name;
	struct Expression *left;
	struct Expression *right;
};

struct Statement
{
	enum StmtType Type;
	struct Expression *Expr;
	struct StatementList *Block;
	struct Statement *Next;
};

struct Program
{
	struct Statements_List *stmts;
};

struct Statements_List
{
	struct Statement *stm;
	struct Statements_List *next;
};
#endif